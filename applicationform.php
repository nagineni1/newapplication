<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Application</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            .displaynone{
                display: none;
            }
        </style>
           <script>   
           $(document).ready(function(){
                $("#profession").change(function() {
                    if($("#profession").val() == 'dealer'){
                        $('.div_dealer').show();
                        $('.div_location').show();
                        
                    } else{
                        $('.div_dealer').hide();
                        $('.div_location').show();
                        }
                    if($("#profession").val() == 'enterprenuer'){
                        $('.div_enterprise').show();
                        $('.div_location').show();
                    } else{
                        $('.div_enterprise').hide();
                        $('.div_location').show();
                    }
                });
               $("#skills").change(function() {
                    if($("#skills").val() == 'yes'){
                        $('.div_skills_group').show();
                        
                    } else{
                        $('.div_skills_group').hide();
                        }
              
                });
               
               $("#shg").change(function() {
                    if($("#shg").val() == 'yes'){
                        $('.div_shg_group').show();
                        
                    } else{
                        $('.div_shg_group').hide();
                        }
              
                });
               
               $("#organisation").change(function() {
                    if($("#organisation").val() == 'yes'){
                        $('.div_organisation_group').show();
                        
                    } else{
                        $('.div_organisation_group').hide();
                        }
              
                });
           });
        </script>
	</head>
	<body>
		<div class="container">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Application</h2>
				</div>
					<form method="post">
				<div class="panel-body">
						<div class="well row">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="name">Full Name:</label>
									<input type="text" class="form-control" name="name" id="name" placeholder="Enter your name">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="father_name">Fathers Name:</label>
									<input type="text" class="form-control" name="father_name" id="father_name" placeholder="Enter your father name">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="mobile">Mobile:</label>
									<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile ">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="email">Email:</label>
									<input type="text" class="form-control" name="email" id="email" placeholder="Email id">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="website">website:</label>
									<input type="text" class="form-control" name="website" id="website" placeholder="URL">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="address">Address:</label>
									<input type="text" class="form-control" name="address" id="address" placeholder="address">
								</div>
							</div>
						</div>
						<div class="well row">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="profession">Your Profession:</label>
										<select class="form-control" name="profession" id="profession">
											<option value="" disabled selected>Choose From the List</option>
											<option  value="dealer">Raw hide Collection Dealer</option>
											<option  value="drum_beater">Drum beater</option>
											<option  value="enterprenuer">Enterprenuer</option>
											<option  value="others">Others</option>
										</select>
									</div>
								</div>
								<div class="col-sm-4 div_dealer displaynone">
									<div class="form-group">
										<label for="dealer">Type of dealer</label>
										<div class="checkbox">
											<label><input type="checkbox" name="dealer" value="goat">goat</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" name="dealer" value="sheep">sheep</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" name="dealer" value="buffallo">buffallo</label>
										</div>
									</div>
								</div>
								<div class="col-sm-4 div_enterprise displaynone">
									<div class="form-group">
										<label for="enterprise">Choose enterprise size</label>
										<select class="form-control" name="enterprise">
											<option value="" disabled>Choose From the List</option>
											<option  value="micro">Micro</option>
											<option  value="small">Small</option>
											<option  value="medium">Medium</option>
										</select>
									</div>
								</div>
								<div class="col-sm-4 div_enterprise displaynone">
									<div class="form-group">
										<label for="enterprise_name">Name of the Enterprise:</label>
										<input type="text" class="form-control" name="enterprise_name" id="enterprise_name" placeholder="Name of the Enterprise">
									</div>
								</div>
								<div class="col-sm-4 div_enterprise displaynone">
									<div class="form-group">
										<label for="employees">Number of employees:</label>
										<input type="text" class="form-control" name="employees" id="employees" placeholder="Name of the Enterprise">
									</div>
								</div>
								<div class="col-sm-4 div_enterprise displaynone">
									<div class="form-group">
										<label for="investment">Investment:</label>
										<input type="text" class="form-control" name="investment" id="investment" placeholder="investment">
									</div>
								</div>
								<div class="col-sm-4 div_enterprise displaynone">
									<div class="form-group">
										<label for="activity">Law of Activity:</label>
										<input type="text" class="form-control" name="activity" id="activity" placeholder="Law of Activity">
									</div>
								</div>
								<div class="col-sm-4 div_location displaynone">
									<div class="form-group">
										<label for="location">Location:</label>
										<input type="text" class="form-control" name="location" id="location" placeholder="Location">
									</div>
								</div>
						</div>
						<div class="well row">
							<h3>Do you have any skills/experience in the leather technologies</h3>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="skills">Choose:</label>
									<select class="form-control" id="skills" name="skills">
										<option value="">Choose From the List</option>
										<option  value="yes">YES</option>
										<option  value="no">NO</option>
									</select>
								</div>
							</div>
							<div class="col-sm-4 displaynone div_skills_group">
								<div class="form-group">
									<label for="skill_name">Name of the skill:</label>
									<input type="text" class="form-control" name="skill_name" id="skill_name" placeholder="skill name">
								</div>
							</div>
                            <div class="col-sm-4 displaynone div_skills_group">
								<div class="form-group">
									<label for="institute_name">Institute Name:</label>
									<input type="text" class="form-control" name="institute_name" id="institute_name" placeholder="institute name">
								</div>
							</div>
							<div class="col-sm-4 displaynone div_skills_group">
								<div class="form-group">
									<label for="course">Course:</label>
									<input type="text" class="form-control" name="course" id="course" placeholder="Enter course name">
								</div>
							</div>
							<div class="col-sm-4 displaynone div_skills_group">
								<div class="form-group">
									<label for="institute_address">Institute Address:</label>
									<input type="text" class="form-control" name="institute_address" id="institute_address" placeholder="Institute address">
								</div>
							</div>
							<div class="col-sm-4 displaynone div_skills_group">
								<div class="form-group">
									<label for="duration">Duration:</label>
									<input type="text" class="form-control" name="duration" id="duration" placeholder="duration">
								</div>
							</div>
						</div>
						<div class="well row">
							<h3>Have you joined in any self help group?</h3>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="shg">Choose</label>
									<select class="form-control" id="shg" name="shg">
										<option value="">Choose From the List</option>
										<option  value="yes">YES</option>
										<option  value="no">NO</option>
									</select>
								</div>
							</div>
							<div class="col-sm-4 div_shg_group displaynone">
								<div class="form-group">
									<label for="shg_name">Name of the SHG:</label>
									<input type="text" class="form-control" name="shg_name" id="shg_name" placeholder="Name of the SHG">
								</div>
							</div>
							<div class="col-sm-4 div_shg_group displaynone">
								<div class="form-group">
									<label for="shg_address">Address:</label>
									<input type="text" class="form-control" name="shg_address" id="shg_address" placeholder="SHG Address">
								</div>
							</div>
						</div>
						<div class="well row">
							<h3>Have you joined in any societies/NGO/Voluntary organisation? </h3>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="organisation">Choose</label>
									<select class="form-control" id="organisation" name="organisation">
										<option value="">Choose From the List</option>
										<option  value="yes">YES</option>
										<option  value="no">NO</option>
									</select>
								</div>
							</div>
							<div class="col-sm-4 displaynone div_organisation_group">
								<div class="form-group">
									<label for="organisation_name">Name of the Organisation:</label>
									<input type="text" class="form-control" name="organisation_name" id="organisation_name" placeholder="Name of the Organisation">
								</div>
							</div>
							<div class="col-sm-4 displaynone div_organisation_group">
								<div class="form-group">
									<label for="organisation_address">Address:</label>
									<input type="text" class="form-control" name="organisation_address" id="organisation_address" placeholder="Organisation Address">
								</div>
							</div>
						</div>
                        
				</div>
                        <div class="panel-footer">
					           <input type="submit" class="btn btn-primary" value="submit">
				    `   </div>
					</form>
			</div>
		</div>
	</body>
</html>